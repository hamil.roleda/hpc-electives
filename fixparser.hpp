#pragma once
#include <string>
#include <unordered_map>

namespace order_parsers {
    class FIXParser {
        public:
            std::unordered_map<int, std::string> parse(const char *message);
            std::string getValue(int tag, std::unordered_map<int, std::string> &umap);
            int tag;
        
        private:
            const char* buf_ {nullptr};
            size_t len_ {0};
    };
}