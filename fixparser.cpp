#include "fixparser.hpp"
#include <iostream>

namespace order_parsers {
    std::unordered_map<int, std::string> FIXParser::parse(const char *message) {
        std::unordered_map<int, std::string> umap;
        while (*message) {
            int tag = 0;
            while (*message && *message != '='){
                tag = tag * 10 + (*message - '0');
                ++message;     
            }
        ++message;
        std::string value;
        while (*message && *message != '\x01'){
            value += *message;
            ++message;
        }
        umap[tag] = value;
        ++message;
        }
        return umap;
    }
    std::string FIXParser::getValue(int tag, std::unordered_map<int, std::string> &umap) {
        auto value = umap.find(tag);
        if (value != umap.end()) {
            return value->second;
        } 
        else {
            return "";
        }
    }
}