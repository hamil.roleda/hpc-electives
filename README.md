# HPC Electives FIXParser

This project is part of the HPC Electives, wherein a FIXParser is to be created.

Currently, the program can parse a FIX message and store the values in an unordered map. From the unordered map, the program also retrieves the given value from a tag which is specified in the `main` program.

## Prerequisites
1. g++ (GCC)

## How to Run
To run the program, first we have to build it.

### Building the Program
Using the given Makefile, run the `make` command to create the `main` program.
```
$ make
```
### Running the program
After creating the main file, run the following:
```
$ ./main
```

Example
```
$ ./main
Value for tag 52: 20190206-16:25:10.403
```

