#include "fixparser.hpp"
#include "filereader.hpp"
#include <iostream>
#include <algorithm>

int main() {
    order_parsers::FIXParser FIXParser_obj;

    const char* file = "fix_message.txt";
    reader::FileReader reader(file);

    const int buf_size = 256;
    char buf[buf_size];

    size_t file_size = reader.read(buf, buf_size);
    
    std::cout<<"File size in bytes: "<<file_size<<std::endl;

    std::unordered_map<int, std::string> parser_map = FIXParser_obj.parse(buf);
    
    int tag = 52;
    std::string tag_value = FIXParser_obj.getValue(tag, parser_map);
    std::cout<<"Value for tag "<<tag<<": "<<tag_value<<std::endl;

    return 0;
}; 