#pragma once
#include <string>

namespace reader {
    class FileReader {
        public:
            FileReader(const char* filename) {
               file_ = fopen(filename, "r");
            }
            ~FileReader() {
                if (file_ != nullptr)
                    fclose(file_);
            }
            size_t read(char* buf, size_t len) {
                return fread(buf, 1, len, file_);
            }

        private:
            FILE* file_ {nullptr};
    };
}